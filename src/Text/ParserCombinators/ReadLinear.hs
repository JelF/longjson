{-# LANGUAGE TupleSections #-}
{-# LANGUAGE LambdaCase #-}

-- | Non-Alternative Parser for lazy monoids
module Text.ParserCombinators.ReadLinear (
  ReadLinear(ReadLinear)
, runReadLinneral
, execReadLinear
, yield

, get
, getN
, getIf
, getWhile

, look
, peek
, test

, munch
, munch1

, matchSkip
, eoi

, readPToLinear
, readPToLinear1
) where

import Control.Monad
import Data.List
import Data.Functor
import Text.ParserCombinators.ReadP (ReadP, readP_to_S)

-- | Parser monad. You can either use this constructor or try
-- to combine 'look', 'get' and 'yield' which are self-documented.
--
-- == Type parameters
-- [input] Generalized input. 'get' implies it to be an array,
-- so you have to define your own 'get's.
-- [output] Generalised parse result.
-- 'ReadLineral' implies it will be a 'Monoid', and 'yield' and 'execReadLinear'
-- implies it is an 'Applicative'.
-- [value] part of 'Functor' interface.
--
-- == Notes
-- As an 'Monad' 'ReadLineral' redefines 'fail'. As required, it is a
-- left zero, however it is not a right zero. You can recover rest of input
-- after fails (and obliviously output will not disapear because of linearity).
newtype ReadLinear input output value = ReadLinear {
  -- | Raw 'ReadLinear' destructor. Prefer 'execReadLinear' if possible.
  -- Otherwise (i.e. output is not an 'Applicative') get care of last coratege
  -- item. If it is 'Left' append something to output to indicate an error.
  -- You can add literaly 'error' in case you do not care.
  runReadLinneral :: input -> (input, output, Either String value)
}

instance (Monoid output) => Functor (ReadLinear input output) where
  fmap = ap . return

instance (Monoid output) => Applicative (ReadLinear input output) where
  pure = return
  (<*>) = ap

instance (Monoid output) => Monad (ReadLinear input output) where
  return value = ReadLinear (, mempty, Right value)
  fail err = ReadLinear (, mempty, Left err)
  a >>= fn = ReadLinear $ \input ->
    let (lInput, lOutput, lEither) = runReadLinneral a input
    in either (\err -> (lInput, lOutput, Left err)) (\lValue ->
      let (rInput, rOutput, rEither) = runReadLinneral (fn lValue) lInput
      in (rInput, lOutput <> rOutput, rEither)
    ) lEither

-- | Preferred 'ReadLinear' descturctor.
--
-- = Parameters
-- [failWith :: (input -> 'String' -> x)]
-- special function to construct last output item when it is ooops.
-- Use @const error@ unless you care.
-- [readLinear :: 'ReadLinear' input (output x) ()] parser to use.
-- [input :: input] An input.
-- [Returns :: output x] `Monoid` (hope it is a lazy monoid) with parse results.
execReadLinear :: (Monoid (output x), Applicative output) =>
  (input -> String -> x) ->
  ReadLinear input (output x) () ->
  input ->
  output x
execReadLinear failWith readLinear input =
  let (input', output, eitherValue) = runReadLinneral readLinear input
  in output <> either (pure . failWith input') (const mempty) eitherValue

-- | Add value to output monoid.
yield :: (Applicative output) => x -> ReadLinear input (output x) ()
yield x = ReadLinear (, pure x, Right ())

-- | Read and remove head of input or fail if end of input reached.
get :: (Monoid output) => ReadLinear [x] output x
get = ReadLinear $ \case
  [] -> ([], mempty, Left "End of input reached")
  (x:xs) -> (xs, mempty, Right x)

-- | Read and remove n elements from begining of input or fail if
-- end of input reached.
getN :: (Monoid output) => Int -> ReadLinear [x] output [x]
getN n = replicateM n get

-- | Read and remove element if it match condition.
getIf :: (Monoid output) => (x -> Bool) -> ReadLinear [x] output (Maybe x)
getIf fn = test fn >>= \isSuccess ->
  if isSuccess then Just <$> get else pure Nothing

-- | Read and remove elements while they match condition.
getWhile :: (Monoid output) => (x -> Bool) -> ReadLinear [x] output [x]
getWhile fn = test fn >>= \isSuccess ->
  if isSuccess then pure (:) <*> get <*> getWhile fn else pure []

-- | Read whole input.
look :: (Monoid output) => ReadLinear input output input
look = ReadLinear $ \input -> (input, mempty, Right input)

-- | Read element or fail if end of input reached.
peek :: (Monoid output) => ReadLinear [x] output x
peek = ReadLinear $ \case
  [] -> ([], mempty, Left "End of input reached")
  (x:xs) -> (x:xs, mempty, Right x)

-- | return True if element exist and match condition.
test :: (Monoid output) => (x -> Bool) -> ReadLinear [x] output Bool
test fn = look >>= \input -> return $ not (null input) && fn (head input)

-- | Destroy elements while they match condition.
munch :: (Monoid output) => (x -> Bool) -> ReadLinear [x] output ()
munch fn = getWhile fn $> ()

-- | Destroy element if it match condition.
munch1 :: (Monoid output) => (x -> Bool) -> ReadLinear [x] output ()
munch1 fn = getIf fn $> ()

-- | Destroy exact string if it is prefix of input or fail otherwise.
matchSkip :: (Monoid output, Eq x) => [x] -> ReadLinear [x] output ()
matchSkip prefix = look >>= \input -> if prefix `isPrefixOf` input
  then getN (length prefix) $> ()
  else fail "No match"

-- | Fail unless end of input reached.
eoi :: (Monoid output) => ReadLinear [x] output ()
eoi = not . null <$> look >>= (`when` fail "End of input expected")

-- | Transform 'ReadP' from "Text.ParserCombinators.ReadP" to 'ReadLinear'.
-- Fails if multiple variants matched. If you want to filter only variants
-- that parse exact part of input use 'readPToLinear1'.
readPToLinear :: (Monoid output) =>
  ReadP x -> ReadLinear String output x
readPToLinear readP = ReadLinear $ \input ->
  let variants = readP_to_S readP input
  in if length variants == 1
    then let (parse, xs) = head variants in (xs, mempty, Right parse)
    else (input, mempty, Left "No parse")

-- | Transform 'ReadP' from "Text.ParserCombinators.ReadP" to 'ReadLinear'.
-- Fails if multiple variants matched entire given string.
readPToLinear1 :: (Monoid output) =>
  ReadP x -> String -> ReadLinear input output x
readPToLinear1 readP input =
  let variants = filter (null . snd) $ readP_to_S readP input
  in if length variants == 1
    then pure $ fst $ head variants
    else fail "No parse"

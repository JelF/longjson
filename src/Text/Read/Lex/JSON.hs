{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}

-- | Linear JSON lexer.
--
-- Compatible with [ECMA-404](https://www.ecma-international.org/publications/standards/Ecma-404.htm).
-- Also see <http://www.json.org/> for human-readable specifications.
--
-- This lexer uses 'List's (and 'String' as a 'List' of 'Char's)
-- as a basic structures to simplify linearity, so convert your
-- "Data.ByteString.Lazy" 'ByteString's or other stuff.
-- An output, @['JSONEvent']@ ,is a raw stream of JSON lexems which can be
-- piped to analyzing tool or lazy json builder
-- (not included in this package yet).
--
-- Internal functions can be exported on request,
-- <https://gitlab.com/JelF/longjson/issues>.
module Text.Read.Lex.JSON (
  JSONEvent(..)
, parseJSON

, toRealFloat
) where

import Data.Char
import Data.List
import Data.Scientific
import Text.Read.Lex

import Text.ParserCombinators.ReadLinear

-- | Sum of all possible JSON Lexems
-- 'Scientific' from "Data.Scientific" is used to store numbers because
-- ECMA-404 does not specify any precision. 'toRealFloat' is re-exported
-- to simplify handling it
--
-- == Events
-- [JSONObjectStart] currency value is an object, next event will be
-- JSONObjectKey.
-- [JSONObjectKey] first event of key-value pair, next event will be one of
-- value events.
-- [JSONObjectEnd] object defenition ended, next event will be same
-- as when regular value event fired.
-- [JSONArrayStart] current value is an array, next events will be values of
-- that array.
-- [JSONArrayEnd] array defenition ended, next event will be same
-- as when regular value event fired.
-- [JSONString] string value, like @bar@ in @{"foo": "bar"}@.
-- [JSONNumber] numeric value, like @3.1415@. see 'toRealFloat'.
-- [JSONBoolean] boolean value, like @true@.
-- [JSONNULL] null value, exactly @null@.
-- [JSONParseSuccess] fired when object successfuly parsed.
-- Guaranteed to be last event fired.
-- [JSONParseError] fired on object parse error.
-- First value is part of input left unparsed, second is error message.
-- Guaranteed to be last event fired.
data JSONEvent
  where
    JSONObjectStart :: JSONEvent
    JSONObjectKey :: String -> JSONEvent
    JSONObjectEnd :: JSONEvent
    JSONArrayStart :: JSONEvent
    JSONArrayEnd :: JSONEvent
    JSONString :: String -> JSONEvent
    JSONNumber :: Scientific -> JSONEvent
    JSONBoolean :: Bool -> JSONEvent
    JSONNULL :: JSONEvent
    JSONParseSuccess :: JSONEvent
    JSONParseError :: String -> String -> JSONEvent
  deriving (Show, Eq)

type ReadJSONLinear = ReadLinear String [JSONEvent]

-- |Tokenizes input 'String' to 'JSONEvent' lexemes.
-- Pretends to take O(1) memory in process.
parseJSON :: String -> [JSONEvent]
parseJSON =
  execReadLinear JSONParseError $ json >> yield JSONParseSuccess

json :: ReadJSONLinear ()
json = jsonValue >> munch isSpace >> eoi

jsonValue :: ReadJSONLinear ()
jsonValue = munch isSpace >> peek >>= jsonValue'
  where
    jsonValue' x
      | x == '{' = jsonObject
      | x == '[' = jsonArray
      | x == '\"' = jsonString >>= yield . JSONString
      | isDigit x = jsonNumber >>= yield . JSONNumber
      | x == '-' = jsonNumber >>= yield . JSONNumber
      | otherwise = jsonWord

jsonObject :: ReadJSONLinear ()
jsonObject = do
    munch isSpace >> matchSkip "{" >> yield JSONObjectStart
    jsonObjectPair
    munch isSpace >> matchSkip "}" >> yield JSONObjectEnd
  where
    jsonObjectPair = do
      munch isSpace >> jsonString >>= yield . JSONObjectKey
      munch isSpace >> matchSkip ":"
      munch isSpace >> jsonValue
      munch isSpace >> getIf (== ',') >>= maybe (pure ()) (const jsonObjectPair)

jsonArray :: ReadJSONLinear ()
jsonArray = do
    munch isSpace >> matchSkip "[" >> yield JSONArrayStart
    jsonArrayElement
    munch isSpace >> matchSkip "]" >> yield JSONArrayEnd
  where
    jsonArrayElement = do
      munch isSpace >> jsonValue
      munch isSpace >> getIf (== ',') >>= maybe (pure ()) (const jsonArrayElement)

jsonString :: ReadJSONLinear String
jsonString = do
    munch isSpace >> matchSkip "\""
    jsonStringContent
  where
    push x = (x:) <$> jsonStringContent
    jsonStringContent = get >>= \case
      '\"' -> pure ""
      '\\' -> get >>= \case
        'n' -> push '\n'
        'r' -> push '\r'
        'b' -> push '\b'
        't' -> push '\t'
        'u' -> getN 4 >>= readPToLinear1 readHexP >>= push . chr
        x -> push x
      x -> push x

jsonNumber :: ReadJSONLinear Scientific
jsonNumber = getWhile (`elem` dictionary) >>= readPToLinear1 scientificP
  where dictionary = "1234567890.e+-"

jsonWord :: ReadJSONLinear ()
jsonWord = look >>= \input ->
  if "true" `isPrefixOf` input then getN 4 >> yield (JSONBoolean True)
  else if "false" `isPrefixOf` input then getN 5 >> yield (JSONBoolean False)
  else if "null" `isPrefixOf` input then getN 4 >> yield JSONNULL
  else fail "No match"

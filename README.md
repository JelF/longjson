# Longjson

Primary target of longjson project is to handle json APIs with long polling
in real time. Another benefit is ability to reduce memory comsumption
of parsing N bytes of JSON to O(1).

This also provides Text.ParserCombinators.ReadLinear, non-Alternative parser
monad

This project is not released yet so you can't get it from hackage.

## Text.ParserCombinators.ReadLinear

Small suite used to write json lexer. Can be imagined as a

```haskell
type ReadLinneral input output value =
    StateT input (WriterT output (Either String value))
```

Note, that because of linearity the only way of error reporting is to add
notice about error to output monoid (possibly as bottom).

Can import ReadP combinators

## Text.Read.Lex.JSON

Linear JSON lexer `String -> [JSONEvent]`. JSONEvent is sum of all json events:
- Begining of object or Array
- Object key (String)
- String (String)
- Number (Data.Sciencific)
- Boolean (Bool)
- NULL
- EOF
- Parse error (Rest json text, error message)

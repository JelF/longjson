import Control.Exception.Base
import Text.Read.Lex.JSON

main :: IO ()
main = do
  putStrLn ""
  testParse "simple"
    "{\"success\": true}"
    [JSONObjectStart,
    JSONObjectKey "success", JSONBoolean True,
    JSONObjectEnd, JSONParseSuccess]
  testParse "unicode"
    "{\"shitty\\tk\\u0451y\": 3.22e10}"
    [JSONObjectStart,
    JSONObjectKey "shitty\tk\1105y", JSONNumber (read "3.22e10"),
    JSONObjectEnd, JSONParseSuccess]
  testParse "array"
    "{\"users\":[{\"id\":1},{\"id\":2},{\"id\":3},{\"id\":4},{\"id\":5},{\"id\":6},{\"id\":7},{\"id\":8},{\"id\":9},{\"id\":10}]}"
    [JSONObjectStart,
    JSONObjectKey "users",JSONArrayStart,
    JSONObjectStart, JSONObjectKey "id", JSONNumber (read "1.0"), JSONObjectEnd,
    JSONObjectStart, JSONObjectKey "id", JSONNumber (read "2.0"), JSONObjectEnd,
    JSONObjectStart, JSONObjectKey "id", JSONNumber (read "3.0"), JSONObjectEnd,
    JSONObjectStart, JSONObjectKey "id", JSONNumber (read "4.0"), JSONObjectEnd,
    JSONObjectStart, JSONObjectKey "id", JSONNumber (read "5.0"), JSONObjectEnd,
    JSONObjectStart, JSONObjectKey "id", JSONNumber (read "6.0"), JSONObjectEnd,
    JSONObjectStart, JSONObjectKey "id", JSONNumber (read "7.0"), JSONObjectEnd,
    JSONObjectStart, JSONObjectKey "id", JSONNumber (read "8.0"), JSONObjectEnd,
    JSONObjectStart, JSONObjectKey "id", JSONNumber (read "9.0"), JSONObjectEnd,
    JSONObjectStart, JSONObjectKey "id", JSONNumber (read "10.0"), JSONObjectEnd,
    JSONArrayEnd,
    JSONObjectEnd,JSONParseSuccess]

testParse :: String -> String -> [JSONEvent] -> IO ()
testParse name source expectation =
  putStrLn ("Testing " <> name) >>
    if parseJSON source == expectation
      then putStrLn "Successfuly done"
      else do
        print $ parseJSON source
        fail "parse result does not match input"

import Control.Exception.Base
import Control.Monad
import Text.ParserCombinators.ReadLinear

main :: IO ()
main = do
  putStrLn ""
  testConcurrency
  testMatchSkip

testConcurrency :: IO ()
testConcurrency =
  let bottom = error "parser have taken string it does not need"
      subject = execReadLinear const $ do
        getN 1 >>= yield
        getN 2 >>= yield
        getN 3 >>= yield
  in do
    putStrLn "Test ReadLinear concurrency..."
    print $ take 1 $ subject $ "1" <> bottom
    print $ take 2 $ subject $ "123" <> bottom
    print $ take 3 $ subject $ "123456" <> bottom
    putStrLn "ReadLinear concurrency test succeed"

testMatchSkip :: IO ()
testMatchSkip =
  let subject = execReadLinear (flip const) $
        matchSkip "foo" >> getN 3 >>= yield
  in do
    putStrLn "Test matchSkip..."
    when (subject "foobar" /= ["bar"]) $ fail "foobar failed"
    when (subject "bazbar" /= ["No match"]) $ fail "bazbar failed"
    putStrLn "matchSkip test success"
